// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "AGS_MenuSystem.h"

#define LOCTEXT_NAMESPACE "FAGS_MenuSystemModule"

void FAGS_MenuSystemModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	
}

void FAGS_MenuSystemModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
	
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FAGS_MenuSystemModule, AGS_MenuSystem)