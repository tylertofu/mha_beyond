/**
*	Author: LazerTale
*	Website: https://lazertale.com
*	Documentation: https://docs.lazertale.com/
*	Copyright � LazerTale 2019. All rights reserved.
*/

#include <AGS_MenuSystem/Public/AGS_MenuSystemBPLibrary.h>

#include "AGS_MenuSystemBPLibrary.h"
#include "AGS_MenuSystem.h"
#include "Engine/UserInterfaceSettings.h"
#include "Engine/Classes/GameFramework/Actor.h"


void UAGS_MenuSystemBPLibrary::ChangeApplicationUIScale(float newScale)
{
	GetMutableDefault<UUserInterfaceSettings>()->ApplicationScale = newScale;
}
