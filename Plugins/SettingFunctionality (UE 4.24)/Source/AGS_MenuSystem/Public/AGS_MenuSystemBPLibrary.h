/**
*	Author: LazerTale
*	Website: https://lazertale.com
*	Documentation: https://docs.lazertale.com/
*	Copyright © LazerTale 2019. All rights reserved.
*/

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "AGS_MenuSystemBPLibrary.generated.h"

/* *
	Function library class.
*/
UCLASS()
class UAGS_MenuSystemBPLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

    /**
        Change Application UMG scale
        This will affect all UMG widgets
    */
	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Change application UI scale", Keywords = "UMG UI Menu System UI Scale"), Category = "AGS|MenuSystem|Utilities")
	static void ChangeApplicationUIScale(float newScale);
};
