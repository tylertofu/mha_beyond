// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "mhaBeyondGameMode.h"
#include "mhaBeyondCharacter.h"
#include "UObject/ConstructorHelpers.h"

AmhaBeyondGameMode::AmhaBeyondGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/_AAA_Characters/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
